import io

from rest_framework import serializers
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer

from .models import Books


# class BooksModel:
#     def __init__(self, title, content):
#         self.title = title
#         self.content = content


class BooksSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Books
        fields = "__all__"



# def encode():
#     model = BooksModel('Master and Margaret', 'Content')
#     model_sr = BooksSerializer(model)
#     print(model_sr.data, type(model_sr.data), sep='\n')
#     json = JSONRenderer().render(model_sr.data)
#     print(json)
#
#
# def decode():
#     stream = io.BytesIO(b'{"title":"Master and Margaret","content":"Content"}')
#     data = JSONParser().parse(stream)
#     serializer = BooksSerializer(data=data)
#     serializer.is_valid()
#     print(serializer.validated_data)